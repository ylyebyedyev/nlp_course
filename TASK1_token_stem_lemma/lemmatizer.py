"""CUSTOM LEMMATIZER"""
import os
import re
import json
import zipfile


# Unpacking raw data
if not os.path.exists("DATA/word_forms.json"):
    with zipfile.ZipFile("DATA/word_forms.zip", "r") as zip_f:
        zip_f.extractall("DATA")

# Path to default data file
ENDINGS_PATH = "DATA\ends.json"


def _get_possible_ends():
    """
    Save once collected possible ending for further usage
    :return:
    """
    result = set()
    data = json.load(open("DATA/word_forms.json"))
    for words in data:
        result.update(recognize_ends(words))
    json.dump(result, open(ENDINGS_PATH), ensure_ascii=False)


def recognize_ends(words):
    """
    HERE YOU SHOULD IMPLEMENT YOUR OWN END[INGS] RECOGNIZER
    :param words: list of str, ["снег", "снега", ..., "снегам", ...]
    :return: set of str, ["а", "ам", ..., "у", ...]
    """
    result = set()

    # TODO: YOUR CODE HERE
    result.update([word[-1] for word in words])

    return result


def word_infinitive(word):
    """
    HERE SHOULD BE IMPLEMENTED SEARCH INFINITIVE FORM OF WORD, BASED ON word_forms.json.
    YOU WILL NEED TO CREATE SOME ADDITIONAL METHODS/FILES TO OPTIMIZE INITIAL DATA FOR OPTIMAL
    SEARCH SPEED.
    :param word: str, single word (example: "снегом")
    :return: str (example: "снег")
    """
    # TODO: YOUR CODE HERE
    result = word

    return result


def custom_lemmatizer(word):
    """
    HERE SHOULD BE YOUR OWN CUSTOM WORD NORMALIZER,
    START WITH ATTEMPT TO LEMMATIZE WORD WITH word_infinitive()
    IF WORD IS OUT OF DICT - STEM IT, BASED ON DATA EXTRACTED WITH recognize_ends()
    :param word: str, single word (example: "ЗИНЗИВЕРОМ")
    :return: str, normalized word (example: "зинзивер")
    """
    if not os.path.exists(ENDINGS_PATH):
        _get_possible_ends()
    endings = json.load(open(ENDINGS_PATH))

    # TODO: YOUR CODE HERE
    result = word_infinitive(word)
    if result == word:
        patterns = re.compile(r"|".join(endings))
        result = patterns.sub("", result)

    return result
