"""IMPLEMENTATION OF CUSTOM TOKENIZER"""
# THIS LIBS CAN BE USEFUL
import re
from string import punctuation


def custom_tokenizer(text):
    """
    HERE SHOULD BE YOUR OWN CUSTOM TOKENIZER
    :param text: str, (example "На словах он Лев Толстой, а на деле...")
    :return: list of str (example ["На", "словах", "он", "Лев", "Толстой", ",", "а", "на", "деле", "..."])
    """
    result = []

    # TODO: YOUR CODE HERE:
    result = text.split()

    return result
