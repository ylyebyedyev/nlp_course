"""EVALUATION OF CUSTOM IMPLEMENTATIONS"""
import json
from nltk.metrics.distance import edit_distance

from TASK1_token_stem_lemma.tokenizer import custom_tokenizer
from TASK1_token_stem_lemma.golden import gold_tokenizer
from TASK1_token_stem_lemma.lemmatizer import custom_lemmatizer
from TASK1_token_stem_lemma.golden import gold_lemmatizer


def _get_score(golden, custom):
    distance = edit_distance(golden, custom)
    score = round(100 - distance / len(golden) * 100, 2)
    return score


def test_tokenizer(text):
    custom = custom_tokenizer(text)
    golden = gold_tokenizer(text)
    return _get_score(golden, custom)


def test_lemmatizer(text):
    tokens = gold_tokenizer(text)
    golden = gold_lemmatizer(tokens)
    custom = [custom_lemmatizer(word) for word in tokens]
    return _get_score(golden, custom)


def check_on_data():
    """
    PLEASE RUN THIS SCRIPT BEFORE SUBMISSION. IT'LL GENERATE SCORES.json WITH SCORE OF YOUR CODE.
    :return:
    """
    TOKENIZER_TEXT = open("DATA/train_tokenizer.txt", encoding="utf-8").read()
    LEMMATIZER_TEXT = open("DATA/train_lemmatizer.txt", encoding="utf-8").read()
    tokenizer_score = test_tokenizer(TOKENIZER_TEXT)
    lemmatizer_score = test_lemmatizer(LEMMATIZER_TEXT)
    json.dump({"tokenizer score": tokenizer_score,
               "lemmatizer_score": lemmatizer_score},
              open("SCORES.json", "w"), indent=2)


if __name__ == "__main__":
    check_on_data()
