"""LIBRARY BASED IMPLEMENTATION FOR COMPARISON WITH CUSTOM DECISIONS"""
from nltk.tokenize import word_tokenize
from pymystem3 import Mystem


def gold_tokenizer(text):
    return word_tokenize(text, language="russian")


def gold_lemmatizer(list_of_words):
    lemmatizer = Mystem()
    result = []
    for word in list_of_words:
        lemma = lemmatizer.lemmatize(word)[0]
        result.append(lemma)
    return result
