PYTHON=python3.6

setup: setup-venv setup-nltk

setup-venv:
	@echo '============================= creating virtual environment ============================='
	[ -d $(venv) ] || { virtualenv -p $(PYTHON) $(venv);}
	. $(venv)/bin/activate  && pip --cache-dir=$(venv) install pip --upgrade && pip --cache-dir=$(venv) install setuptools --upgrade && pip --cache-dir=$(venv) install --upgrade -r requirements.txt

setup-nltk:
    . venv/bin/activate

    echo "Loading NLTK data"
    $PYTHON -m nltk.downloader 'punkt'

setup-spacy:
    . venv/bin/activate

    echo "Loading SpaCy model"
    $PYTHON -m spacy download en en_core_web_lg