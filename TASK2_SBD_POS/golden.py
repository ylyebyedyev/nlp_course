import os
import spacy

PATH = "DATA"


class SpaCyModel:
    """
    Lazy loader for SpaCy
    """
    __SM = None

    @classmethod
    def load(cls):
        if not cls.__SM:
            cls.__SM = cls()
        return cls.__SM

    def __init__(self):
        self.parser = spacy.load("en_core_web_lg")

    def __call__(self, text):
        return self.parser(text)


def iter_data():
    """
    Iterate over text files
    :return: tuple(str (file name), SpaCy.Doc)
    """
    SPACY = SpaCyModel.load()
    for file in os.listdir(PATH):
        text = open(os.path.join(PATH, file), encoding="utf-8").read()
        yield file, SPACY(text.replace("\n", " "))
