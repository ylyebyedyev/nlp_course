"""Evaluate your decision"""
import os
import json

from TASK2_SBD_POS.golden import iter_data, PATH
from TASK2_SBD_POS.sbd import sentences


def _gen_spacy_sents():
    """
    SBD with Spacy
    :return: dict, key filename, value - list of strings
    """
    result = set()
    for file, doc in iter_data():
        sents = [sent.text.strip() for sent in doc.sents]
        result.update(sents)
    return result


def _golden_sents():
    """
    Original sentences
    :return: dict, key filename, value - list of strings
    """
    result = set()
    for file in os.listdir(PATH):
        text = open(os.path.join(PATH, file), encoding="utf-8").read().split("\n")
        result.update([sent.strip() for sent in text])
    return result


def _custom_sents():
    """
    Sentences from custom SBD
    :return: dict, key filename, value - list of strings
    """
    result = set()
    customs = sentences()
    for doc in customs:
        result.update(customs[doc])
    return result


def check_on_data():
    """
    Lets test your code!
    :return:
    """
    golden = _golden_sents()
    print("Processing with Spacy")
    spacy = _gen_spacy_sents()
    print("Processing with your approach")
    custom = _custom_sents()

    spacy_guess = golden & spacy
    custom_guess = golden & custom
    custom_spacy = custom & spacy

    json.dump({"SPACY GUESS": "%s/%s" % (len(spacy_guess), len(golden)),
               "YOUR GUESS": "%s/%s" % (len(custom_guess), len(golden)),
               "YOU ARE LIKE SPACY": "%s/%s" % (len(custom_spacy), len(spacy))},
              open("SCORE.json", "w"), indent=2)


if __name__ == "__main__":
    check_on_data()
