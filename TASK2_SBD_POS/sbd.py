"""Implementation of custom sentence boundary detection"""
import json
import math
import torch
from pytorch_pretrained_bert import BertTokenizer, BertForMaskedLM
from collections import defaultdict

from TASK2_SBD_POS.golden import iter_data

# Please fill free to play with other models:
# bert-base-uncased
# bert-large-uncased
# bert-base-multilingual
MODEL = "bert-base-cased"
BERT = BertForMaskedLM.from_pretrained(MODEL)
BERT.eval()
TOKENIZER = BertTokenizer.from_pretrained(MODEL, do_lower_case=False)


def _get_score(sentence):
    """
    Estimate probability of the given sentence
    :param sentence: str, sentence candidate
    :return: float, probability that current string is a sentence. Closer to 0 is better.
    """
    tokenize_input = TOKENIZER.tokenize(sentence)
    tensor_input = torch.tensor([TOKENIZER.convert_tokens_to_ids(tokenize_input)])
    predictions = BERT(tensor_input)
    loss_fct = torch.nn.CrossEntropyLoss()
    loss = loss_fct(predictions.squeeze(), tensor_input.squeeze()).data
    return math.exp(loss)

# To see how _get_score works try this example
# print([_get_score(text) for text in ["She has", "She has a cat", "She has a cat I have", "She has a cat I have a dog"]])


def custom_sbd_recognizer(doc):
    """
    Iterates over SpaCy.Tokens (https://spacy.io/api/token), recognise starting and ending tokens of the sentence
    :param doc: SpaCy.Doc object, see https://spacy.io/api/doc
    :return: list of tuples, tuple is 2 int, id of starting and ending token of the sentence [(0, 5), (6, 12), ...]
    """
    result = []

    # TODO: YOUR CODE IS HERE
    start_id = 0
    while start_id < len(doc):
        end_id = start_id + 2

        # Avoid prediction single token sentence
        try:
            base_prob = _get_score(doc[start_id: end_id].text)
        except IndexError:
            end_id += 1
            base_prob = _get_score(doc[start_id: end_id].text)

        end_id += 1

        # Avoid prediction single token sentence
        try:
            next_prob = _get_score(doc[start_id: end_id].text)
        except IndexError:
            end_id += 1
            next_prob = _get_score(doc[start_id: end_id].text)

        while base_prob / next_prob < 2:
            if end_id == len(doc):
                break
            end_id += 1
            next_prob = _get_score(doc[start_id: end_id].text)
        result.append((start_id, end_id))
        start_id = end_id + 1

    return result


def sentences():
    """
    Convert integer borders into real text sentences
    :return: dict, keys is str (golden file title), value is a list of str (recognized sentences)
    """
    result = defaultdict(list)
    for file, doc in iter_data():
        borders = custom_sbd_recognizer(doc)

        # lets convert recognized borders to sentences
        for strt, end in borders:
            end += 1
            result[file].append(doc[strt: end].text.strip())
    return result


if __name__ == "__main___":
    sents = sentences()
    print(json.dumps(sents, indent=3))
